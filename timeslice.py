#! /usr/bin/python3

import os
import math
import sys
from PIL import Image

# Workaround python + mintty + windows print buffering
def log(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)
    sys.stderr.flush()

mask_image = None
result_image_data = None

result_image_size = (1, 1)
image_count = 1

# Special arguments to various blend functions.
# all
arg_smooth = 0.0
# radial*
arg_radial_center = [0.5, 0.5]
# slanted
arg_slanted_degree = -1

# Cached values for various blend functions
radial_center = [0, 0]
radial_max_radius = 1
slanted_normal = (1, 0)

# The timeslice functions take xy coordinates and the index of the image among all images.
# The function returns a number in [0, 1] telling at what percentage the pixel should be blended.

def blend(start, end, x, i):
    if arg_smooth == 0.0:
        return 1.0 if x >= start and x < end else 0.0
    else:
        ramp_distance = (end - start) * arg_smooth
        rampup_start = start - ramp_distance / 2.0
        rampup_end = start + ramp_distance / 2.0
        rampdown_start = end - ramp_distance / 2.0
        rampdown_end = end + ramp_distance / 2.0

        if x < rampup_start or x > rampdown_end:
            return 0.0
        elif x <= rampup_end:
            return (x - rampup_start) / ramp_distance if i > 0 else 1.0
        elif x >= rampdown_start:
            return (rampdown_end - x) / ramp_distance if i + 1 < image_count else 1.0

        return 1.0

def timeslice_horizontal(coords, i):
    start_x = i * result_image_size[0] / image_count
    end_x = (i + 1) * result_image_size[0] / image_count

    return blend(start_x, end_x, coords[0], i)

def timeslice_vertical(coords, i):
    start_y = i * result_image_size[1] / image_count
    end_y = (i + 1) * result_image_size[1] / image_count

    return blend(start_y, end_y, coords[1], i)

def timeslice_radial_linear(coords, i):
    start_radius = i * radial_max_radius / image_count
    end_radius = (i + 1) * radial_max_radius / image_count

    dx = coords[0] - radial_center[0]
    dy = coords[1] - radial_center[1]
    rsq = dx * dx + dy * dy

    return blend(start_radius, end_radius, math.sqrt(rsq), i)

def timeslice_radial(coords, i):
    max_rsq = radial_max_radius * radial_max_radius

    start_rsq = i * max_rsq / image_count
    end_rsq = (i + 1) * max_rsq / image_count

    dx = coords[0] - radial_center[0]
    dy = coords[1] - radial_center[1]
    rsq = dx * dx + dy * dy

    return blend(start_rsq, end_rsq, rsq, i)

def timeslice_slanted(coords, i):
    # Let's call diagonal 1 the one that goes from (0, 0) to (w, h)
    # and diagonal 2 the one that goes from (0, h) to (w, 0).
    if arg_slanted_degree <= 90:
        start_corner = (0, 0)
        end_corner = result_image_size
    elif arg_slanted_degree <= 180:
        start_corner = (result_image_size[0], 0)
        end_corner = (0, result_image_size[1])
    elif arg_slanted_degree <= 270:
        start_corner = result_image_size
        end_corner = (0, 0)
    else:
        start_corner = (0, result_image_size[1])
        end_corner = (result_image_size[0], 0)

    def point_line_distance(origin, normal, point):
        return (point[0] - origin[0]) * normal[0] + (point[1] - origin[1]) * normal[1]

    end_to_start = point_line_distance(start_corner, slanted_normal, end_corner)
    point_to_start = point_line_distance(start_corner, slanted_normal, coords)

    start = i * end_to_start / image_count
    end = (i + 1) * end_to_start / image_count

    return blend(start, end, point_to_start, i)

def blend_image(image, blend_func, i):
    for y in range(result_image_size[1]):
        for x in range(result_image_size[0]):
            blend_value = blend_func((x, y), i)
            if blend_value > 0:
                pixel_value = image.getpixel((x, y))
                coord = y * result_image_size[0] + x
                prev_value = result_image_data[coord]
                blended_value = tuple([prev_value[i] + pixel_value[i] * blend_value for i in range(3)])
                result_image_data[coord] = blended_value

blend_func = timeslice_horizontal
if len(sys.argv) >= 2:
    if sys.argv[1] == '-h' or sys.argv[1] == '--help':
        log('Usage: timeslice.py blendmode [smooth [percent]]')
        #log('       timeslice.py maskimage')
        log('')
        log('Possible blendmode values:')
        log('  horizontal: default')
        log('  vertical')
        log('  radial_linear [h [v]]: h and v for center are between 0 and 100 (default 50)')
        log('  radial [h [v]]: h and v for center are between 0 and 100 (default 50)')
        log('  slanted [N]: N is a number in degrees (default is automatic)')
        log('')
        #log('If maskimage is given, the color values in this image are used to')
        #log('create the timeslice, with values from RGB 0, 0, 0 through 99, 99, 99,')
        #log('which are 1000000 possibilities, selecting the image (and their blend)')
        #log('')
        sys.exit(0)
    elif sys.argv[1] == 'horizontal':
        blend_func = timeslice_horizontal
    elif sys.argv[1] == 'vertical':
        blend_func = timeslice_vertical
    elif sys.argv[1] == 'radial_linear':
        blend_func = timeslice_radial_linear
        try:
            if len(sys.argv) >= 3:
                arg_radial_center[0] = int(sys.argv[2]) / 100.0
            if len(sys.argv) >= 4:
                arg_radial_center[1] = int(sys.argv[3]) / 100.0
        except ValueError:
            pass
    elif sys.argv[1] == 'radial':
        blend_func = timeslice_radial
        try:
            if len(sys.argv) >= 3:
                arg_radial_center[0] = int(sys.argv[2]) / 100.0
            if len(sys.argv) >= 4:
                arg_radial_center[1] = int(sys.argv[3]) / 100.0
        except ValueError:
            pass
    elif sys.argv[1] == 'slanted':
        blend_func = timeslice_slanted
        try:
            if len(sys.argv) >= 3:
                arg_slanted_degree = int(sys.argv[2]) % 360
        except ValueError:
            pass
    elif sys.argv[1] != 'smooth':
        log('Using mask file', sys.argv[1])
        mask_image_file = sys.argv[1]
        mask_image = Image.open(mask_image_file)

if sys.argv[-1] == 'smooth':
    arg_smooth = 1.0
if len(sys.argv) >= 2 and sys.argv[-2] == 'smooth':
    arg_smooth = int(sys.argv[-1]) / 100.0

# List jpeg files in the current directory (excluding previous results)
jpeg_files = sorted([f for f in os.listdir('.')
                     if (os.path.splitext(f)[1].lower() == '.jpg'
                      or os.path.splitext(f)[1].lower() == '.jpeg')
                     and not '__timeslice_result.jpg' in f])

# Look at each jpeg file
image_count = len(jpeg_files)
if image_count == 1:
    log('Only one jpeg file found, no point in creating a timeslice')
    sys.exit(0)

for i in range(image_count):
    log('Processing image', jpeg_files[i])

    image = Image.open(jpeg_files[i])

    # On the first image, create the result image with the same mode and size
    if result_image_data is None:
        result_image_size = image.size
        result_image_data = [(0, 0, 0) for i in range(image.size[0] * image.size[1])]

        # if no arg was given to slanted, calculate one automatically
        if arg_slanted_degree == -1:
            arg_slanted_degree = math.atan(float(image.size[1]) / image.size[0]) * 180 / math.pi

        # Cache invariant values
        radial_center = (int(arg_radial_center[0] * image.size[0]),
                         int(arg_radial_center[1] * image.size[1]))
        farthest_corner = (result_image_size[0] if arg_radial_center[0] < 0.5 else 0,
                           result_image_size[1] if arg_radial_center[1] < 0.5 else 0)
        center_to_corner = (radial_center[0] - farthest_corner[0],
                            radial_center[1] - farthest_corner[1])
        radial_max_radius = math.sqrt(center_to_corner[0] * center_to_corner[0] +
                                      center_to_corner[1] * center_to_corner[1])
        slanted_angle_rad = arg_slanted_degree * math.pi / 180.0
        slanted_normal = (math.cos(slanted_angle_rad), math.sin(slanted_angle_rad))

    # Blend the image
    blend_image(image, blend_func, i)

if result_image_data is None:
    log('No .jpg or .jpeg images found')
else:
    result_image = Image.new(image.mode, image.size)
    image_data = [tuple([int(round(c)) for c in p]) for p in result_image_data]
    result_image.putdata(image_data)
    result_image.save('__timeslice_result.jpg', quality=95)
